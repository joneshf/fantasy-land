# Conformant Implementations

Here are a list of implementations that live in Fantasy Land:

* [bacon.js](https://github.com/raimohanska/bacon.js) implements
  Monad and Functor for EventStream and Property on the "fantasy-land" branch
* [aljebra](https://github.com/markandrus/aljebra) implements common
  Monoid structures from Haskell
* ECMAScript 5 provides a Semigroup and Functor for Array

Conforming implementations are encouraged to promote the Fantasy Land logo:

![](logo.png)
